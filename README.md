## Why 

There are many OpenStreetMap communities across Africa, with varying capacity levels, some are active, some are dormant, some are very just beginning, and in some countries there are no existing communities at all. All these communities can learn from each other and grow. A continent wide monthly mapathon will help grow cohesion across countries on the continent and grow the spirit of collaboration while contributing data to OpenStreetMap.

## How
Each of the 12 mapathons will be hosted by a different country, who will also choose the place and purpose of the mapping and will give a short presentation about their community at the beginning of the mapathon.

After the presentation will break out into the following rooms according to language and mapping experience. There will be a room for beginners mappers to learn how to map, and a room for experienced mappers to do validation, and mapping.

Registration for the event will be done via Eventbrite in different languages to ensure it can reach many people, and a call for mapathon hosts will be shared in advance to schedule events for the whole year in advance. 



| Time | Activity |
| -------- | -------- | 
|14:00 - 14.30 UTC     | Introduction to the Mapathon, Presentation from the host country |
|14:30 - 16:30 UTC    | Breakout Rooms - Training and Mapping     | 
|16:30 - 17:00 UTC     | Networking  | 


## When
The Mapathons will be held monthly every last Wednesday of the month.
* Time: 3 Hours
    * UTC: 14:00 - 17:00 hrs
    * West Africa: 15.00 - 18.00 hrs
    * Central Africa: 16:00 - 19:00 hrs
    * East Africa: 17:00 - 20:00 hrs

## Who
The participants in the mapathon will be OpenStreetMap Community members from all over Africa. Participants will be tracked to see the level of activity from the different countries and help mobilize participants from other countries.


Potential groups to reach out to
* OSM Africa - A network of OSM communities in Africa
* Existing OSM Communities in each country
* YouthMappers chapters
* Organizations who use OpenStreetMap eg Red Cross, MSF, Government Agencies, Private Sector companies, Wider Open Data groups, etc
* Partner organizations who want to contribute to or use OpenStreetMap data

Coordination of the mapathon will be done by **OSM Africa** with support from the HOT Eastern and Southern Africa hub. The mapathon organization team will be set up to take lead on marketing the event, reaching out to potential hosts, identifying areas for mapping, participant registration, collecting participants feedback, and coordinating the different breakout sessions.
